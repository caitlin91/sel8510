%\documentclass[ALC_8110_ALL.tex]{subfiles}
%\documentclass[12pt,a4paper]{article}
\documentclass[12pt,a4paper]{scrartcl}
\usepackage[latin1]{inputenc}
\usepackage{xcolor}
\usepackage{dvipscol}
\usepackage{tipa}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{soul}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{nameref}
\usepackage{pdfpages}
\usepackage{csvsimple}
\usepackage{float}
\usepackage{pdfpages}
\usepackage[section]{placeins}
\usepackage{harvard}
%\usepackage[utf8]{inputenc}
%\usepackage[headsepline]{scrlayer-scrpage}
%\usepackage{blindtext}% dummy text
%\usepackage{subfiles}
%\renewcommand{\familydefault}{\sfdefault}
%\pagecolor{yellow}



\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
%\DeclareRobustCommand*\cal{\@fontswitch\relax\mathcal}
%\DeclareRobustCommand*\mit{\@fontswitch\relax\mathnormal}

%\setlength{\columnsep}{25pt}
%\setlength{\columnseprule}{0.4pt}


\title{Where is Received Pronunciation? - Variation and Change in non-regional Sociolects}
\subtitle{School: English Literature, Language and Linguistics \\ Supervisors: Dr Danielle Turton (SELLL) and Dr Ghada Khattab (ECLS)}
\author{Student Number: 140102208}
\date{\today}


\begin{document}
	
%	\renewcommand*\pagemark{{\usekomafont{pagenumber}Page\nobreakspace\thepage}}
%	\addtokomafont{pageheadfoot}{\upshape}
%%	\onlyinsubfile{
%		\maketitle
%		\begin{center}
%			Word Count:
%		\end{center}
%		\pagebreak
%	%\notinsubfile{this only appears if main.tex is compiled (not when chapter1.tex is compiled)}
	
	
	
	%\lhead{ALC8110}
	%\rhead{Student Number: 140102208}
	%\cfoot{\thepage}
	
	%\clearpairofpagestyles
%	\ohead{Student Number: 140102208}
%	\ihead{Research Proposal}
%	\cfoot{\thepage}
%	\onehalfspacing
	
	\section{Introduction} \label{PropIntro}
	Despite the high profile of Received Pronunciation (henceforth RP) in both the academic and popular spheres, very little recent research has focused on changes in the accent itself or the specifics of variation in pronunciation. The unique position that RP holds as a non-regional sociolect, rather than an area-based dialect is accepted and taught but has not been considered in depth, with much of the existing study accepting southern English standards as exemplary of RP. The existence of this accent raises the question of how a language variety can exist when its speakers are hundreds of miles apart. This research will test the geographical boundaries of RP by analysing speech from speakers in the North East in contrast to southern speakers, providing innovative understanding of the nature of non-regional accents and the speech communities they are comprised of.
	
	\section{Research Questions \& Rationale} \label{PropRQs}
	\begin{enumerate}
		\item \textbf{Descriptive aim} - What pronunciation differences exist between north-eastern and southern speakers of RP, particularly focussing on vowels and diphthongs?
		\item \textbf{Social aim} - What insights does this give on the nature of a non-regional sociolect made up of diffuse speech communities?
		\item \textbf{Theoretical aim} - Can the variation seen in RP shed light on the inter-actions between phonetics, phonology, and morphology during a sound change in progress?
	\end{enumerate}
	
	The proposed research aims to answer the above questions by analysing current variation in RP, building on work by \citeasnoun{Wells1982b} and \citeasnoun{Trudgill2002}, who have provided past overviews of the phonology of the accent and Fabricius \citeyear{Fabricius2000,Fabricius2002a,Fabricius2002b,Fabricius2002c}, who, amongst other things, has covered the stigma of t-glottaling. Particular focus will be on differences in vowel pronunciation between north-eastern and southern speakers in order to cast light on how an accent can be made up of diffuse speech communities which have little to no interaction with each other. The principle of an accent existing within a speech community is well established in theories of language variation and change (Labov 2001) and Wolfram and Schillings-Estes (1998) observe that speaker contact is an important factor in the development of dialects, but RP has long been considered a non-regional dialect without any consideration of how variation and changes can occur without interaction between speakers. The proposed study will also answer a call by \citeasnoun{Britain2017} for an empirical study of what accents the elites of England speak; this will be discussed further in section \ref{method}.
	
	In their discussion of American English, \cite{Wolfram1998} observe that in many ways social status and ethnicity may play a far larger role than region in causing language variation, with African American Vernacular English (henceforth AAVE) being a classic example. Much work has been done on the case of AAVE (including Wolfram and Schillings-Estes 1998 and Wolfram and Thomas 2002) showing unique features, regardless of where in the country it is spoken. This work will inform and assist the discussion of RP as a sociolect, with consistent unique features wherever it is spoken.
	
	\citename{Halfacre2017} investigates the \textsc{goat} vowel, considering how the diphthong varies in different phonological environments (e.g. whether or not it precedes a lateral: \textit{goat} vs. \textit{goal}) and how this interacts with the morpho-syntactic environment (e.g. mono-morphemic \textit{holy} vs. bi-morphemic \textit{hole-y}). Results indicate that the different vowel preceding a lateral (\textit{goal}) is spreading through the grammar of the language, following the lifecycle of phonological processes as outlined by Bermúdez-Otero \citeyear{Bermudez-Otero1999,Bermudez-Otero2011,Bermudez-Otero2015}. This study is constrained to a small number of speakers from southern regions and uses word lists rather than having access to vernacular speech via sociolinguistic interviews, giving the opportunity for precise comparison but less insight into change in the speech community. Further study in the proposed project will build on this work by comparing speakers from different regions to discover similar variation and changes and identifying where these are happening, with a particular focus on whether different speech communities are undergoing the same changes at the same time. The phonetic data of words with different morphological structures will also make steps towards answering the question of the effects of morphology on phonetics \cite{Strycharczuk2016} and how that interacts with changes in progress.
	
	\section{Data \& Methodology} \label{PropMethod}
	
	The methods used will be predominantly quantitative.
	Data will be collected by a combination of standard sociolinguistic interviews and elicitation techniques such as wordlists and minimal pairs. Sociolinguistic interviews allow researchers to collect data closest to the vernacular usage \cite{Tagliamonte2006}. This will be supplemented with elicited forms to ensure a sufficient number of occurrences of the required sound in the necessary context. These interviews will be transcribed, which will allow for acoustic analysis.
	
	To avoid researcher bias based on making a judgement about a person's accent before recording them, the participants will be recruited on the basis of socio-economic class, beginning with speakers who have had a private school education (including public and independent schools). Speakers will be asked details about their and their family's education and background as well as the more standard questions about occupation. This will serve to make a more detailed analysis of socio-economic class than is common in linguistic studies (e.g. \citeasnoun{Baranowski2017}). Social class has been classified in many ways including a simple occupation-based division "into blue-collar workers and white-collar labor" \cite[p. 303]{Baranowski2017}, the official governmental system NS-SEC \citeyear{NSSEC2001}, and a more recent classification based on lifestyle and current choices \cite{Savage2013}. However, none of these take into account the critical period hypothesis \textbf{(ref)}, that a person's language develops in their childhood and many of their phonological features are solidified by adulthood, therefore, their socio-economic background in childhood is a better indication of their language than their class at the time of recording. This gap is particularly suprising because many linguistic studies (e.g. \citeasnoun{Baranowski2017}) base a person's regionality on where they grew up, not where they live at the time of recording. The childhood element of the socio-economic class analysis is particularly pertinent to the study of RP because this accent has roots in the public school systems of England (\cite{Fabricius2000,Jones1917}).
	This improved measure and analysis of the socio-economic class variable will add a layer of detail to the discussion of research question two. RP, as discussed in sections \ref{Intro} and \ref{RQs}, is considered non-regional accent, based on the model of class-based variation seen in figure \ref{triangle}.
	\begin{figure}[h]
		\includegraphics[width=1.0\linewidth]{../img/EnglishVariationTriangle.png}
		\caption{Relation between social and regional accents in England (adapted from \protect\cite{Wells1982a})} \label{triangle}
	\end{figure}
	
	Praat analysis \cite{Praat} has been available for some time but until recently measurements had to be taken manually, requiring many hours to extract significant numbers of measurements. The addition of forced alignment software, FAVE \cite{FAVE}, allows for quick analysis of far larger data sets. An additional methodological aim of this research is to test this software, which has been gaining traction in sociolinguistic study for the analysis of big data sets. FAVE is originally designed for American dialects and has not been used extensively on British data (examples include \citeasnoun{Mackenzie2013}). \citeasnoun{Bailey2016} shows the beginning of the development of a British pronouncing dictionary to automate coding of three British consonant variables, however, many vowels are likely to be encountered in a study of RP which have not yet been automated for FAVE analysis hence investigation will be required into how to apply a tool for American dialects to the British vowel system.
	
	Extraction via FAVE will produce formant measurements for the vowels of interest and these will be statistically analysed using Rstudio. The current gold standard for statistical analysis in phonetics is Generalised Additive Mixed Models (GAMMs e.g. \citeasnoun{Baayen2016} and \citeasnoun{Soskuthy2017}), and this analysis will include regression and mixed effect models but will also implements GAMMs to analyse dynamic formant measurements (measurements taken at time points throughout the vowel, rather than just once in the middle), which will provide robust statistical evidence for research questions one and three.
	
	\section{Conclusion} \label{PropConc}
	In conclusion, this research has descriptive, social and theoretical contributions. It looks into not only the current state of phonological variation and change in the Received Pronunciation accent but due to the unique perspective allowed by the location of Newcastle University it will give innovative information on the existence, or lack thereof, of a non-regional sociolect and the speech communities that this is comprised of, taking into account both social class and regional factors. The analysis of variation by morpho-syntactic context, will also provide valuable theoretical input to the ongoing debate surrounding the interactions between phonetics, phonology, and morphology during a sound change in progress
	
	
	
	
	
	
	\pagebreak
	\bibliographystyle{agsm}
	\bibliography{/Users/Caitlin/OneDrive/Documents/Documents/bibFiles/library}
	
	\appendix
	\pagebreak
	
	
\end{document}