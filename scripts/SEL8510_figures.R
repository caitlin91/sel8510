## READ IN DATA ####
source("scripts/SEL8510_DataCleaning.R")
library(Cairo)
library(LaCroixColoR)

data_figs = data_clean
data_figs_TB = data_clean_TB %>%
  mutate(schoolRegion = factor(schoolRegion, levels=c("South_East", "North_East"))) %>%
  mutate(follSeq = recode(follSeq, "none" = "none", "oneSyll" = "oneSyll", "twoSyll" = "twoSyll", "complxcoda" = "complxcoda", "compcoda_onesyll" = "compcoda_sylls", "compcoda_twosyll" = "compcoda_sylls")) %>%
  mutate(follSeq_small = recode(follSeq, "none" = "none", "oneSyll" = "Syll", "twoSyll" = "Syll", "complxcoda" = "complxcoda", "compcoda_sylls" = "Syll")) %>%
  mutate(log_dur = log(dur))
data_figs_FS = data_clean_FS

# The palette with grey:
cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# The palette with black:
cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# themes
# theme_F2 <- scale_y_continuous(breaks=seq(0,3500, 500), limits=c(0,3500)) +
#   theme_bw() +
#   theme(legend.position = "none") +

## INITIAL FIGURES ####

footstrut_F1_plot = ggplot(data_figs_FS, aes(x = lexicalSet, y = norm_F1, fill = lexicalSet)) +
  geom_boxplot() + 
  #geom_jitter() +
  NULL
footstrut_F1_plot

strut_F1_plot = ggplot(data_figs_FS %>% filter(lexicalSet == "STRUT"), aes(x = speakerNumber, y = norm_F1, fill = speakerNumber)) +
  geom_boxplot() +
  #geom_jitter() +
  NULL
strut_F1_plot

footstrut_F2_plot = ggplot(data_figs_FS, aes(x = lexicalSet, y = norm_F2, fill = lexicalSet)) +
  geom_boxplot() + 
  geom_jitter() +
  NULL
footstrut_F2_plot

strut_F2.plot = ggplot(data_figs_FS %>% filter(lexicalSet == "STRUT"), aes(x = speakerNumber, y = norm_F2, fill = lexicalSet)) +
  geom_boxplot() +
  #geom_jitter() +
  NULL
strut_F2.plot

## TRAP_BATH ####

TB_F2.plot = ggplot(data_figs_TB, aes(x=lexicalSet, y=norm_F2, fill = lexicalSet)) +
  geom_boxplot() +
  facet_wrap("schoolRegion") +
  scale_y_continuous(breaks=seq(0,3500, 250), limits=c(0,3500)) +
  scale_fill_brewer(palette="Set1") +
  theme_bw() +
  theme(legend.position = "none") +
  ggtitle("F2 of ᴛʀᴀᴘ and ʙᴀᴛʜ words") +
  ylab("F2 (Hz)") +
  xlab("Lexical Set") +
  #geom_jitter(colour = "gray") +
  NULL
TB_F2.plot
Cairo(file="figures/TBF2.png", 
      type="png",
      units="in", 
      width=8, 
      height=4, 
      pointsize=12*2*96/72, 
      dpi=96)
TB_F2.plot
dev.off()

trap_f2.plot = ggplot(data_figs_TB %>% filter(lexicalSet == "TRAP"), aes(x=schoolRegion, y=norm_F2, fill = lexicalSet)) +
  geom_boxplot() +
  scale_y_continuous(breaks=seq(0,3500, 500), limits=c(0,3500)) +
  theme_bw() +
  #geom_jitter() +
  NULL


TB_dur.plot = ggplot(data_figs_TB, aes(x = lexicalSet, y = log_dur, fill = lexicalSet)) +
  geom_boxplot() +
  facet_wrap(~schoolRegion) +
  #geom_jitter() +
  #stat_smooth() +
  #geom_abline(intercept = 0.108121, slope = 0.007531) +
  #geom_violin() +
  theme_bw() +
  scale_fill_brewer(palette = "Set1") +
  theme(legend.position = "none") +
  ggtitle("Duration of ᴘᴀʟᴍ, ᴛʀᴀᴘ and ʙᴀᴛʜ words") +
  xlab("Lexical Set") +
  ylab("log(duration)") +
  scale_y_continuous(limits = c(-3.5,0)) +
  NULL
TB_dur.plot
Cairo(file="figures/TBdur.png", 
      type="png",
      units="in", 
      width=8, 
      height=5, 
      pointsize=12*2*96/72, 
      dpi=96)
TB_dur.plot
dev.off()



TB_dur_NE.plot = ggplot(data_figs_TB %>% filter(schoolRegion == "North_East"), aes(x = lexicalSet, y = dur, fill = lexicalSet)) +
  geom_boxplot() +
  #geom_jitter() +
  #stat_smooth() +
  #geom_abline(intercept = 0.108121, slope = 0.007531) +
  #geom_violin() +
  scale_y_continuous(limits = c(0,1.0)) +
  NULL
TB_dur_NE.plot

TB_dur_SE.plot = ggplot(data_figs_TB %>% filter(schoolRegion == "South_East"), aes(x = lexicalSet, y = dur, fill = lexicalSet)) +
  geom_boxplot() +
  #geom_jitter() +
  #stat_smooth() +
  #geom_abline(intercept = 0.108121, slope = 0.007531) +
  #geom_violin() +
  scale_y_continuous(limits = c(0,1.0)) +
  NULL
TB_dur_SE.plot


BATH_F2_follSeq.plot = ggplot(data_figs_TB %>% filter(lexicalSet == "BATH"), aes(x=follSeq_small, y=norm_F2, fill = follSeq_small)) +
  geom_boxplot() +
  #geom_violin() +
  scale_y_continuous(breaks=seq(0,3500, 250), limits=c(0,3500)) +
  theme_bw() +
  #scale_fill_brewer(palette="Set1") +
  scale_fill_manual(values = c(lacroix_palette("PassionFruit", type = "discrete"))) +
  theme(legend.position = "none") +
  ggtitle("F2 of ʙᴀᴛʜ words by following sequence for speakers educated in the North-East") +
  ylab("F2 (Hz)") +
  xlab("Following Sequence") +
  #geom_jitter() +
  NULL
BATH_F2_follSeq.plot
Cairo(file="figures/BATHF2follSeq.png", 
      type="png",
      units="in", 
      width=8, 
      height=5, 
      pointsize=12*2*96/72, 
      dpi=96)
BATH_F2_follSeq.plot
dev.off()

## Social Figures ####
## School Category
tb_F2_schCat = ggplot(data_figs_TB, aes(x=schoolCat2, y = norm_F2)) +
  geom_boxplot() +
  #facet_wrap(~childhoodRegion)+
  NULL
tb_F2_schCat

## School Region
TB_F2_schReg.plot =  ggplot(data_figs_TB, aes(x=lexicalSet, y = norm_F2, fill = lexicalSet)) +
  geom_boxplot() +
  #geom_violin() +
  scale_y_continuous(breaks=seq(0,3500, 250), limits=c(0,3500)) +
  theme_bw() +
  scale_fill_brewer(palette="Set1") +
  theme(legend.position = "none") +
  ggtitle("F2 of ᴛʀᴀᴘ and ʙᴀᴛʜ words for speakers educated in the North-East") +
  ylab("F2 (Hz)") +
  xlab("Lexical Set") +
  #geom_jitter() +
  NULL
TB_F2_schReg.plot
Cairo(file="figures/TBF2NE.png", 
      type="png",
      units="in", 
      width=8, 
      height=4, 
      pointsize=12*2*96/72, 
      dpi=96)
TB_F2_schReg.plot
dev.off()


bath_F2_schlReg.plot = ggplot(data_figs_TB %>% filter(lexicalSet == "BATH"), aes(x=schoolRegion, y = norm_F2, fill = schoolRegion)) +
  geom_boxplot() +
  #geom_violin() +
  scale_y_continuous(breaks=seq(0,3500, 250), limits=c(0,3500)) +
  theme_bw() +
  scale_fill_brewer(palette="Dark2") +
  theme(legend.position = "none") +
  ggtitle("F2 of ʙᴀᴛʜ words by School Region") +
  ylab("F2 (Hz)") +
  xlab("School Region") +
  #geom_jitter() +
  NULL
Cairo(file="figures/bathF2schlReg.png", 
      type="png",
      units="in", 
      width=5, 
      height=4, 
      pointsize=12*96/72, 
      dpi=96)
bath_F2_schlReg.plot
dev.off()



# Childhood Region
tb_F2_chregion = ggplot(data_figs_TB, aes(x=childhoodRegion, y = norm_F2, fill = childhoodRegion)) +
  geom_boxplot() +
  NULL
tb_F2_region

bath_F2_chregion = ggplot(data_figs_TB %>%
                            filter(lexicalSet == "BATH"), aes(x=childhoodRegion, y = norm_F2, fill = childhoodRegion)) +
  geom_boxplot() +
  NULL
bath_F2_chregion

# School Region
tb_F2_schRegion = ggplot(data_figs_TB, aes(x = schoolRegion, y = norm_F2, fill = schoolRegion)) +
  geom_boxplot() +
  scale_y_continuous(breaks=seq(0,3500, 500), limits=c(0,3500)) +
  theme_bw() +
  NULL
tb_F2_schRegion

# SE only
bath_F2_SE = ggplot(data_figs_TB %>% filter(schoolRegion == "South_East") %>% filter(lexicalSet == "BATH"), aes(x=word, y=norm_F2)) +
  geom_boxplot() +
  #geom_violin() +
  # scale_y_continuous(breaks=seq(0,3500, 500), limits=c(0,3500)) +
  # theme_bw() +
  # theme(legend.position = "none") +
  # #ggtitle("") +
  # ylab("F2 (Hz)") +
  # #xlab("School Region") +
  # #geom_jitter() +
  NULL
bath_F2_SE



## FOOT~STRUT ####

FS_F1.plot = ggplot(data_figs_FS, aes(x= lexicalSet, y=norm_F1, fill = lexicalSet)) +
  geom_boxplot() +
  scale_y_continuous(breaks=seq(0,1500, 100), limits=c(0,1500)) +
  theme_bw() +
  scale_fill_brewer(palette = "Set1") +
  theme(legend.position = "none") +
  ggtitle("F1 of ꜰᴏᴏᴛ and sᴛʀᴜᴛ words") +
  ylab("F1 (Hz)") +
  xlab("Lexical Set") +
  NULL
FS_F1.plot
Cairo(file="figures/FSF1.png", 
      type="png",
      units="in", 
      width=8, 
      height=4, 
      pointsize=12*2*96/72, 
      dpi=96)
FS_F1.plot
dev.off()

FS_F1_schReg.plot = ggplot(data_figs_FS, aes(x= lexicalSet, y=norm_F1, fill = lexicalSet)) +
  geom_boxplot() +
  facet_wrap("schoolRegion") +
  scale_y_continuous(breaks=seq(0,1500, 100), limits=c(0,1500)) +
  theme_bw() +
  scale_fill_brewer(palette = "Set1") +
  theme(legend.position = "none") +
  ggtitle("F1 of ꜰᴏᴏᴛ and sᴛʀᴜᴛ words, by School Region") +
  ylab("F1 (Hz)") +
  xlab("Lexical Set") +
  NULL
FS_F1_schReg.plot
Cairo(file="figures/FSF1SchR.png", 
      type="png",
      units="in", 
      width=8, 
      height=4, 
      pointsize=12*2*96/72, 
      dpi=96)
FS_F1_schReg.plot
dev.off()

FS_F1_allspeakers.plot = ggplot(data_figs_FS, aes(x= lexicalSet, y=norm_F1, fill = schoolRegion)) +
  geom_boxplot() +
  facet_wrap(~speakerNumber, ncol=5) +
  #facet_grid(schoolRegion) +
  scale_y_continuous(breaks=seq(0,1500, 100), limits=c(0,1500)) +
  theme_bw() +
  scale_fill_brewer(palette = "Dark2") +
  theme(legend.text = element_text("schoolRegion")) +
  #theme(legend.position = "none") +
  ggtitle("F1 of ꜰᴏᴏᴛ and sᴛʀᴜᴛ words, by speaker") +
  ylab("F1 (Hz)") +
  xlab("Lexical Set") +
  NULL
FS_F1_allspeakers.plot
Cairo(file="figures/FSF1allspeakers.png", 
      type="png",
      units="in", 
      width=8, 
      height=4, 
      pointsize=12*2*96/72, 
      dpi=96)
FS_F1_allspeakers.plot
dev.off()

FS_schR.plot = ggplot(data_figs_FS, aes(x= lexicalSet, y=norm_F1, fill = schoolRegion)) +
  geom_boxplot() +
  #geom_jitter() +
  facet_wrap("schoolRegion") +
  scale_y_continuous(breaks=seq(0,1500, 250), limits=c(0,1500)) +
  NULL
FS_schR.plot