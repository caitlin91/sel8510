---
title: "SEL8510_stats_diary"
author: "Caitlin Halfacre"
date: "16 August 2018"
output:
  html_document: default
---
```{r}
load("16Aug2018_env.Rdata")
library(tidyverse)
``` 

# 16th August 2018
Talked through models with Danielle this morning. Spent day coding linguistic factors then working on bath_F2 models. The best model currently is:
bath_F2.mod.current = lmer(norm_F2 ~ schoolRegion + follMan + follVc + follSeq + (1|speakerNumber) +(1|word), data_stats_TB %>% filter(lexicalSet == "BATH"))

```{r}
bath_F2.mod.current
summary(bath_F2.mod.current)
print(ranef.current)
```
Anovas showed that follPlace and preSeg weren't helpful so were left out, worth retesting this though. Need to keep an eye on correlations between fllMn/fllVc - prob because all nasal are voiced.
On running a model with social factors only it was found that the school Region had a far bigger effect than anything else and hence possibly masked other social factors so was included on its own. **TODO** - re-run social factors only on NEsch speakers to look for effects there
