\contentsline {chapter}{\numberline {1}Abstract}{2}
\contentsline {chapter}{\numberline {2}Introduction}{4}
\contentsline {chapter}{\numberline {3}Literature Review}{5}
\contentsline {section}{\numberline {3.1}Social Class}{5}
\contentsline {subsection}{\numberline {3.1.1}Social Class and Language}{5}
\contentsline {subsubsection}{\nonumberline Speech Communities}{6}
\contentsline {subsection}{\numberline {3.1.2}An aside - speaker mobility}{7}
\contentsline {subsection}{\numberline {3.1.3}Social Class and Education}{7}
\contentsline {section}{\numberline {3.2}Received Pronunciation}{7}
\contentsline {section}{\numberline {3.3}\textsc {Foot}$\sim $\textsc {Strut} Distinction}{7}
\contentsline {subsection}{\numberline {3.3.1}\textsc {Foot} Vowel}{7}
\contentsline {subsection}{\numberline {3.3.2}\textsc {Strut} Vowel}{7}
\contentsline {subsection}{\numberline {3.3.3}\textsc {Foot}$\sim $\textsc {Strut} Split}{8}
\contentsline {subsection}{\numberline {3.3.4}Present day situation of the \textsc {Foot}$\sim $\textsc {Strut} Distinction}{8}
\contentsline {section}{\numberline {3.4}\textsc {Trap}$\sim $\textsc {Bath} Distinction}{10}
\contentsline {subsection}{\numberline {3.4.1}\textsc {Trap}}{10}
\contentsline {subsection}{\numberline {3.4.2}\textsc {Bath}}{10}
\contentsline {subsection}{\numberline {3.4.3}The \textsc {Trap}$\sim $\textsc {Bath} Split}{10}
\contentsline {subsection}{\numberline {3.4.4}Present day situation of the \textsc {Trap}$\sim $\textsc {Bath} Distinction}{11}
\contentsline {chapter}{\numberline {4}Methodology \& Method}{15}
\contentsline {section}{\numberline {4.1}Interview design}{15}
\contentsline {section}{\numberline {4.2}Data extraction}{15}
\contentsline {section}{\numberline {4.3}title}{15}
\contentsline {chapter}{\numberline {5}Data Analysis \& Discussion}{17}
\contentsline {chapter}{\numberline {6}Conclusion}{18}
\contentsline {chapter}{\nonumberline Bibliography}{19}
\contentsline {chapter}{\numberline {A}Acknowledgements}{22}
\contentsline {chapter}{\numberline {B}Interview Schedule}{23}
\contentsline {chapter}{\numberline {C}Word List \& Minimal Pairs list}{24}
\contentsline {chapter}{\numberline {D}Data}{25}
